const JSONReporter = require("jasmine-json-test-reporter");
const startServer = require("./tests/server");

let server;

/* start a temporary express server */
function beforeLaunch() {
	return startServer().then((instance) => {
		server = instance;
	});
}

async function onPrepare() {
	const addr = server.address();
	browser.baseUrl = `http://localhost:${addr.port}`;

	/* eslint-disable-next-line no-console -- expected to log */
	console.log(`\nServer listening on: ${browser.baseUrl}\n`);

	/* save results as json */
	jasmine.getEnv().clearReporters();
	const caps = await browser.getCapabilities();
	jasmine.getEnv().addReporter(
		new JSONReporter({
			file: `tests/${caps.get("browserName")}-results.json`,
			beautify: true,
			indentationLevel: 4,
		}),
	);

	/* setup jasmine support for plugin */
	require("./jasmine");
}

function afterLaunch() {
	server.close();
}

exports.config = {
	suites: {
		all: "tests/test.spec.js",
	},

	baseUrl: undefined /* set from onPrepare */,
	framework: "jasmine2",
	directConnect: true,

	capabilities: {
		browserName: "chrome",
		"moz:firefoxOptions": {
			args: ["-headless"],
		},
		chromeOptions: {
			args: ["--no-sandbox", "--disable-gpu", "--headless"],
		},
	},

	plugins: [
		{
			path: "index.js",
			config: {
				rules: {
					"require-sri": "off",
				},
			},
		},
	],

	beforeLaunch,
	onPrepare,
	afterLaunch,
};
