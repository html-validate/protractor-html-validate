# html-validate-protractor test code

Testing is done in two steps:

1. Run protractor using chrome and firefox
2. Verify results from protractor, i.e. some tests are expected to fail and some are expected to pass.

It also contains a small sample application and server (express).

## Test setup

The `/pages` directory contains the source code for a simple application.

Protractor uses `onPrepare` to start an express server hosting the application,
see `server.js`.

## Protractor

Protractor starts the server and runs tests from `test.spec.js`. Results are
ignored but saved to `chrome-results.json` and `firefox-results.json`
respectively.

- Use `npm run test:chrome` to run with chrome
- Use `npm run test:firefox` to run with firefox

## Verification

Jest is used to read `chrome-results.json` and `firefox-results.json` and verify
the expected outcomes. Jest runs tests from `chrome.spec.js` and
`firefox.spec.js` but both of those only are glue to bind the json-report with
the actual tests in `results.spec.js`.

**Tests are mapped between protractor and jest by the spec name, i.e. they must be
named the same in both files!**

## New tests

To add a new test:

1. Optional: edit the application.
2. Add protractor test code in `test.spec.js` (both negative and positive tests)
3. Add result verification to `results.spec.js` (validate the desired outcome of the protractor test)
