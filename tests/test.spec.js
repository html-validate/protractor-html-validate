/* eslint-disable protractor/no-describe-selectors, protractor/no-get-in-it -- technical debt */

const EC = protractor.ExpectedConditions;
const VALID_FILE = "/valid.html";
const INVALID_FILE = "/invalid.html";
const SLOW_FILE = "/slow.html?delay=1000";

/**
 * Run protractor against local webserver.
 *
 * Some of these tests will fail and the results will be written to
 * `protractor-result.json` which `results.spec.js` will read and verify the
 * expected results.
 */
describe("protractor-html-validate", () => {
	const testComponent = element(by.css("test-component p"));
	const slowComponent = element(by.css("slow-component p"));
	const toggleInvalid = element(by.id("toggle-invalid"));
	const changeLocation = element(by.tagName("change-location-component"));

	it("should not report error when a valid page is loaded", () => {
		browser.get(VALID_FILE);
		expect(testComponent.getText()).toEqual("Test component");
	});

	it("should not report error when a slow page is loaded (sync must remain intact)", () => {
		browser.get(SLOW_FILE);
		expect(slowComponent.getText()).toEqual("Slow component data");
	});

	it("should report error when invalid page is loaded", () => {
		browser.get(INVALID_FILE);
	});

	it("should report error if page is invalid when test is finished", () => {
		browser.get(VALID_FILE);
		toggleInvalid.click();
	});

	it("should not report error if page is valid at start and beginning", () => {
		browser.get(VALID_FILE);
		toggleInvalid.click();
		toggleInvalid.click();
	});

	it("should support manual validation", () => {
		browser.get(VALID_FILE);
		toggleInvalid.click();
		expect(browser.htmlvalidate()).toBeValid();
		toggleInvalid.click();
	});

	it("should support page without angular synchronization", () => {
		browser.get(VALID_FILE);
		changeLocation.click();
		browser.wait(
			EC.urlContains("/nonexistingpage"),
			3000,
			"Waits for the URL to contain /nonexistingpage",
		);
	});
});
