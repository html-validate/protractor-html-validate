const express = require("express");
const serveStatic = require("serve-static");

/**
 * Delay middleware: append `?delay=NN` to any request to delay handling by `NN`
 * milliseconds.
 */
function delayMiddleware(req, res, next) {
	const delay = req.url.match(/delay=(\d+)/);
	if (delay) {
		const ms = parseInt(delay[1], 10);
		setTimeout(next, ms);
	} else {
		next();
	}
}

function startServer(port) {
	/* eslint-disable-next-line sonarjs/x-powered-by -- used for tests only */
	const app = express();
	return new Promise((resolve, reject) => {
		app.use(delayMiddleware);
		app.use(serveStatic("node_modules/angular"));
		app.use(serveStatic("pages"));
		const server = app.listen(port || 0, "localhost", (err) => {
			if (err) {
				reject(err);
			} else {
				resolve(server);
			}
		});
	});
}

async function devServer() {
	const server = await startServer(8080);
	const addr = server.address();
	const url = `http://${addr.address}:${addr.port}`;
	/* eslint-disable-next-line no-console -- expected to log */
	console.log(`Started server on ${url}`);
}

if (require.main === module) {
	devServer();
}

module.exports = startServer;
