/* load protractor results and create a flat object with full spec name -> spec */
/* eslint-disable-next-line import/no-dynamic-require, security/detect-non-literal-require -- not under user control */
const protractorResults = require(global.resultsFilename); /* set from {chrome,firefox}.spec.js */
const protractorSpecs = Object.values(protractorResults).reduce((out, suite) => {
	for (const spec of suite.specs) {
		out[spec.fullName] = normalize(spec);
	}
	return out;
}, {});

function normalize(spec) {
	spec.failedExpectations = spec.failedExpectations.map((failure) => {
		failure.message = failure.message
			/* strip trailing whitespace */
			/* eslint-disable-next-line sonarjs/slow-regex -- false positive? or im missing something */
			.replace(/[ \t]+$/gm, "")
			/* normalize self-closing, chrome does different things on different
			 * versions */
			.replace(/ \/>/g, ">");
		/* "Details: $URL" is only present in later versions, until 4.10 is the
		 * earliest version supported this is removed from the output */
		//.replace(/^.*Details:.*https:\/\/html-validate.org.*\n/gm, "");
		return failure;
	});
	return spec;
}

describe("protractor-html-validate", () => {
	let protractorSpec;

	beforeAll(() => {
		/* for each spec load the corresponding result from protractor */
		/* eslint-disable-next-line jest/no-jasmine-globals -- technical debt */
		jasmine.getEnv().addReporter({
			specStarted(spec) {
				protractorSpec = protractorSpecs[spec.fullName];
			},
		});
	});

	beforeEach(() => {
		/* eslint-disable-next-line jest/no-standalone-expect -- technical debt */
		expect(protractorSpec).toBeDefined();
	});

	it("should not report error when a valid page is loaded", () => {
		expect.assertions(2);
		expect(protractorSpec.status).toBe("passed");
	});

	it("should not report error when a slow page is loaded (sync must remain intact)", () => {
		expect.assertions(2);
		expect(protractorSpec.status).toBe("passed");
	});

	it("should report error when invalid page is loaded", () => {
		expect.assertions(5);
		expect(protractorSpec.status).toBe("failed");
		expect(protractorSpec.failedExpectations).toHaveLength(
			2,
		); /* two since it is invalid when test is finished */
		expect(protractorSpec.failedExpectations[0].message).toMatchSnapshot();
		expect(protractorSpec.failedExpectations[1].message).toMatchSnapshot();
	});

	it("should report error if page is invalid when test is finished", () => {
		expect.assertions(4);
		expect(protractorSpec.status).toBe("failed");
		expect(protractorSpec.failedExpectations).toHaveLength(1);
		expect(protractorSpec.failedExpectations[0].message).toMatchSnapshot();
	});

	it("should not report error if page is valid at start and beginning", () => {
		expect.assertions(2);
		expect(protractorSpec.status).toBe("passed");
	});

	it("should support manual validation", () => {
		expect.assertions(4);
		expect(protractorSpec.status).toBe("failed");
		expect(protractorSpec.failedExpectations).toHaveLength(1);
		expect(protractorSpec.failedExpectations[0].message).toMatchSnapshot();
	});

	it("should support page without angular synchronization", () => {
		expect.assertions(2);
		expect(protractorSpec.status).toBe("passed");
	});
});
