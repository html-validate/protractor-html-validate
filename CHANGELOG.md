# protractor-html-validate changelog

## 6.0.0 (2024-12-23)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v18 or later
- **deps:** require html-validate v8 or later

### Features

- **deps:** require html-validate v8 or later ([9917d9d](https://gitlab.com/html-validate/protractor-html-validate/commit/9917d9d62285872cda82352c80b477af3b7fcd27))
- **deps:** require nodejs v18 or later ([d21a078](https://gitlab.com/html-validate/protractor-html-validate/commit/d21a078e1b135dd8185d89d3435f4b6385094984))
- **deps:** support html-validate v9 ([ffd599b](https://gitlab.com/html-validate/protractor-html-validate/commit/ffd599b16a0e6590d28bbdeef63f45e3b3043d40))

## [5.0.0](https://gitlab.com/html-validate/protractor-html-validate/compare/v4.0.0...v5.0.0) (2023-06-04)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v16 or later

### Features

- **deps:** require nodejs v16 or later ([2548ed8](https://gitlab.com/html-validate/protractor-html-validate/commit/2548ed8248fe9bcec0c65f435e4c805a606f8a3f))

### Dependency upgrades

- **deps:** support html-validate v8 ([d1e5f48](https://gitlab.com/html-validate/protractor-html-validate/commit/d1e5f48a33028aadfb432965b4dfcfb37a3a4708))

## [4.0.0](https://gitlab.com/html-validate/protractor-html-validate/compare/v3.0.0...v4.0.0) (2023-04-02)

### ⚠ BREAKING CHANGES

- **deps:** minimum required html-validate version is v5

### Features

- **deps:** minimum required html-validate version is v5 ([fea97a1](https://gitlab.com/html-validate/protractor-html-validate/commit/fea97a1387cbc241c75a32d6bacbaa6b908e6b46))

## [3.0.0](https://gitlab.com/html-validate/protractor-html-validate/compare/v2.0.2...v3.0.0) (2022-05-08)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([6121507](https://gitlab.com/html-validate/protractor-html-validate/commit/61215074b7b756d828ddb6092f1ddd500e698b20))

### Dependency upgrades

- **deps:** update dependency html-validate to v7 ([545aa91](https://gitlab.com/html-validate/protractor-html-validate/commit/545aa910bb16ffd25cacc5d2934ce088500de6c9))

### [2.0.2](https://gitlab.com/html-validate/protractor-html-validate/compare/v2.0.1...v2.0.2) (2021-09-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v6 ([42a9374](https://gitlab.com/html-validate/protractor-html-validate/commit/42a93745ffd3107db1a1a39c7ba7373947926a58))

### [2.0.1](https://gitlab.com/html-validate/protractor-html-validate/compare/v2.0.0...v2.0.1) (2021-06-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v5 ([fc214e4](https://gitlab.com/html-validate/protractor-html-validate/commit/fc214e468123a175780c4e9b931c31275584247d))

## [2.0.0](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.8.0...v2.0.0) (2021-06-20)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([84a8bc8](https://gitlab.com/html-validate/protractor-html-validate/commit/84a8bc88076144e0328ffad8a21632f03ce39b1e))

## [1.8.0](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.8...v1.8.0) (2020-11-08)

### Features

- html-validate v4 compatibility ([27a9c82](https://gitlab.com/html-validate/protractor-html-validate/commit/27a9c82a0f8b4fee566fbab288b58dcee975a436))

## [1.7.8](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.7...v1.7.8) (2020-11-01)

## [1.7.7](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.6...v1.7.7) (2020-10-24)

## [1.7.6](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.5...v1.7.6) (2020-04-05)

### Bug Fixes

- **deps:** html-validate 2.20.0 ([89e0761](https://gitlab.com/html-validate/protractor-html-validate/commit/89e076125b683c3c5b80222f1e5fba06b7c9a186))

## [1.7.5](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.4...v1.7.5) (2020-03-29)

## [1.7.4](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.3...v1.7.4) (2020-02-17)

### Bug Fixes

- **config:** use new void rules ([64d24e9](https://gitlab.com/html-validate/protractor-html-validate/commit/64d24e9161a8ab9b2584ca6f15fe653628d2872f))

## [1.7.3](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.2...v1.7.3) (2020-02-17)

## [1.7.2](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.1...v1.7.2) (2020-01-11)

## [1.7.1](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.7.0...v1.7.1) (2020-01-11)

# [1.7.0](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.6.3...v1.7.0) (2019-11-27)

### Features

- html-validate 2 compatibility ([4c99828](https://gitlab.com/html-validate/protractor-html-validate/commit/4c998286249af9ac67916e1960b413f715a3b6b6))

## [1.6.3](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.6.2...v1.6.3) (2019-10-13)

### Bug Fixes

- use `browser.driver` when accessing page to avoid sync issues on non-angular pages ([716f0ff](https://gitlab.com/html-validate/protractor-html-validate/commit/716f0ff))

## [1.6.2](https://gitlab.com/html-validate/protractor-html-validate/compare/v1.6.1...v1.6.2) (2019-08-20)

### Bug Fixes

- **deps:** update dependency deepmerge to v4 ([e9abb3f](https://gitlab.com/html-validate/protractor-html-validate/commit/e9abb3f))

## 1.6.1 (2019-06-07)

- Bump `html-validate` to 1.1.0

## 1.6.0 (2019-02-17)

- Bump `html-validate` to 0.21.0
- Bump dependencies.

## 1.5.0 (2019-01-29)

- Bump `html-validate` to 0.20.0

## 1.4.0 (2019-01-20)

- Run tests against firefox as well.
- Use `codeframe` formatter to give more context.
- Bump `html-validate` to 0.17.0.

## 1.3.0 (2018-11-21)

- Extend `htmlvalidate:document` by default.
- Bump `html-validate to 0.15.0.
- Bump other dependencies.

## 1.2.2 (2018-10-22)

- Bump `html-validate` to 0.13.0.

## 1.2.1 (2018-10-14)

- Bump `html-validate` to 0.11.1.
- Bump other dependencies.

## 1.2.0 (2018-09-22)

- Change errors so all errors are listed, showing line and column information
  and prefixing with `html-validate` so the source of the errors are easier to
  identify.
- Disable `no-inline-style` by default as some frameworks rely on inline style
  which causes many false positives.

## 1.1.1

- Fix configuration merging.

## 1.1.0

- Bump `html-validate` to 0.10.0

## 1.0.0

- Initial version
