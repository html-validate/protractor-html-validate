(function () {
	const testComponent = {
		template: `<p>Test component</p>
<button id="toggle-invalid" type="button" ng-click="showInvalid = !showInvalid" ng-init="showInvalid = false">Show invalid</button>
<div ng-if="showInvalid">
<button><p>Invalid permitted permitted content</p></button>
</div>
`,
	};

	const slowComponent = {
		template: `<p ng-bind="$ctrl.slow"></p>`,
		controller($http) {
			this.$onInit = () => {
				$http.get("data.json?delay=1000").then((response) => {
					this.slow = response.data.text;
				});
			};
		},
	};

	const invalidComponent = {
		template: `<button type="button"><p>Invalid permitted permitted content</p></button>`,
	};

	const hiddenComponent = {
		template: `<div>hidden text</div>`,
		controller($element) {
			this.$onInit = () => {
				$element.find("div")[0].style.display = "none";
			};
		},
	};

	const changeLocationComponent = {
		template: `<button type="button" ng-click="$ctrl.onClick()">Change location</button>`,
		controller($window) {
			this.onClick = () => {
				$window.location.href = "/nonexistingpage";
			};
		},
	};

	angular
		.module("app", [])
		.component("testComponent", testComponent)
		.component("slowComponent", slowComponent)
		.component("invalidComponent", invalidComponent)
		.component("hiddenComponent", hiddenComponent)
		.component("changeLocationComponent", changeLocationComponent);
})();
