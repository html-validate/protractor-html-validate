const config = {
	extends: ["html-validate:recommended", "html-validate:document"],

	rules: {
		/* firefox does not retain doctype when returning the source code */
		"missing-doctype": "off",

		/* the browser will often do what it wants, out of users control */
		"void-style": "off",
		"no-self-closing": "off",

		/* some frameworks (such as jQuery) often uses inline style, e.g. for
		 * showing/hiding elements */
		"no-inline-style": "off",

		/* scripts will often add markup with trailing whitespace */
		"no-trailing-whitespace": "off",

		/* browser might normalize boolean attributes */
		"attribute-boolean-style": "off",
	},
};

/* istanbul ignore next: we dont test this logic with unittests, the createConfig is covered instead */
function canResolve(pkg) {
	try {
		require.resolve(pkg);
		return true;
	} catch {
		return false;
	}
}

/* use legacy void rule if the new rules dont exist */
const haveVoidContent =
	canResolve("html-validate/build/rules/void-content") /* html-validate  < 4.x */ ||
	canResolve("html-validate/dist/rules/void-content"); /* html-validate >= 4.x */
if (!haveVoidContent) {
	config.rules.void = "off";
	delete config.rules["void-style"];
	delete config.rules["no-self-closing"];
}

module.exports = config;
