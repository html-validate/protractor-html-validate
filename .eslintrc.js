require("@html-validate/eslint-config/patch/modern-module-resolution");

module.exports = {
	extends: ["@html-validate"],

	overrides: [
		{
			files: "pages/app.js",
			extends: ["@html-validate/angularjs"],
		},
		{
			files: "tests/results.spec.js",
			extends: ["@html-validate/jest"],
		},
		{
			files: "tests/test.spec.js",
			extends: ["@html-validate/protractor"],
		},
	],
};
