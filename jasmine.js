/* handle pre html-validate 2.20 where formatter functions are exported directly
 * as functions, not as default */
function importDefault(mod) {
	return mod && mod.__esModule ? mod.default : mod;
}

function importFormatter() {
	/* html-validate v5 */
	try {
		const { formatterFactory } = require("html-validate");
		if (formatterFactory) {
			return formatterFactory("codeframe");
		}
	} catch {
		/* do nothing */
	}

	/* html-validate v4 */
	try {
		return importDefault(require("html-validate/dist/formatters/codeframe"));
	} catch {
		/* do nothing */
	}

	/* html-validate < 4 */
	return importDefault(require("html-validate/build/formatters/codeframe"));
}

const codeframe = importFormatter();

function toBeValid() {
	return {
		compare,
	};
}

function compare(report, context) {
	const result = { pass: report.valid };
	if (report.valid) {
		result.message = "Expected validation errors but there was none";
	} else {
		const errors = codeframe(report.results);
		const capitalized = context ? `: ${context[0].toUpperCase()}${context.substr(1)}` : "";
		const prefix = `html-validate${capitalized}:\n`;
		result.message = `${prefix}${errors}`;
	}
	return result;
}

/* add jasmine matchers */
beforeAll(() => {
	jasmine.addMatchers({
		toBeValid,
	});
});

/* validate page when leaving test */
afterEach(() => {
	expect(browser.htmlvalidate()).toBeValid("after test finished");
});
